//
//  MasterViewController.swift
//  DetailSwitchTips
//
//  Created by See.Ku on 2014/09/27.
//  Copyright (c) 2014 AxeRoad. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

	override func awakeFromNib() {
		super.awakeFromNib()
		if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
		    self.clearsSelectionOnViewWillAppear = false
		    self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// 選択されたCellのreuseIdentifierにあわせて、画面を遷移
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let cell = tableView.cellForRowAtIndexPath(indexPath)
		let id = cell?.reuseIdentifier
		let vc = storyboard?.instantiateViewControllerWithIdentifier(id!) as UIViewController

		// NavigationItemを移植
		var item = vc.navigationItem
		if let nc = vc as? UINavigationController {
			item = nc.topViewController.navigationItem
		}

		item.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
		item.leftItemsSupplementBackButton = true

		// ViewControllerを変更
		splitViewController?.showDetailViewController(vc, sender: self)

		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}
}

